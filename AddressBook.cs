﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace AddressBook
{
    public partial class AddressBook : Form
    {
        List<Entry> Book;
        List<TextBox> textBoxes;
        private View view;
        private Controller controller;
        private FileManager fileManager;

        public AddressBook()
        {
            InitializeComponent();

            Book = new List<Entry>();
            view = new View(bookDisplay);
            textBoxes = new List<TextBox>();

            InitTextBoxes();
            controller = new Controller(textBoxes);

            fileManager = new FileManager();
            
        }

        private void InitTextBoxes()
        {
            textBoxes.Add(fNameTextBox);
            textBoxes.Add(lNameTextBox);
            textBoxes.Add(addressTextBox);
            textBoxes.Add(cityTextBox);
            textBoxes.Add(stateTextBox);
            textBoxes.Add(zipTextBox);
            textBoxes.Add(phoneNumTextBox);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            fileManager.Save(Book);
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            view.ClearDisplay();
            Entry temp = CreateEntry();
            if (temp.GetErrors() == "None")
            {
                Book.Add(temp);
                controller.ClearInput();
            }
            else
            {
                //Find room in form to display error messages to the user
            }

            foreach (Entry ent in Book)
            {
                view.UpdateDisplay(ent);
            }         

        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            view.ClearDisplay();
            List<Entry> results = new List<Entry>();
            Entry searchQuery = new Entry();

            string empty = "/";
            #region ErrorChecking

            string fName, lName, address;
            string city, state, zip, pNum;

            if (addressTextBox.Lines.Length < 1)
                address = empty;
            else
                address = addressTextBox.Lines[0];

            if (cityTextBox.Lines.Length < 1)
                city = empty;
            else
                city = cityTextBox.Lines[0];

            if (stateTextBox.Lines.Length < 1)
                state = empty;
            else
                state = stateTextBox.Lines[0];

            if (zipTextBox.Lines.Length < 1)
                zip = empty;
            else
                zip = zipTextBox.Lines[0];

            if (fNameTextBox.Lines.Length < 1)
                fName = empty;
            else
                fName = fNameTextBox.Lines[0];

            if (lNameTextBox.Lines.Length < 1)
                lName = empty;
            else
                lName = lNameTextBox.Lines[0];

            if (phoneNumTextBox.Lines.Length < 1)
                pNum = empty;
            else
                pNum = phoneNumTextBox.Lines[0];
            #endregion


            searchQuery = searchQuery.CreateSearchQuery( fName, lName, address, city, state, zip, pNum );
            foreach (Entry entry in Book)
            {
                if(entry.CompareEntry(searchQuery))
                {
                    results.Add(entry);
                    view.UpdateDisplay(entry);
                }
            }
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            Book = fileManager.Load();
            view.UpdateDisplay(Book);
        }

        private Entry CreateEntry()
        {
            Entry temp = new Entry();
            
            temp.SetAddress(addressTextBox.Lines[0], cityTextBox.Lines[0], stateTextBox.Lines[0], zipTextBox.Lines[0]);
            temp.SetName(fNameTextBox.Lines[0], lNameTextBox.Lines[0]);
            temp.SetPhoneNumber(phoneNumTextBox.Lines[0]);
           
            return temp;
        }
        
    }
}
