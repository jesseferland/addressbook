﻿using System;

public class Class1
{
    private struct Name
    {
        string first;
        string last;
    };

    private struct Address
    {
        int streetNumber;
        string streetName;
        string city;
        string state;
        int zipcode;
    };

    private char[] PhoneNumber;

	public Class1()
	{
        PhoneNumber = new char[10];
	}

    public void SetName(string f, string l)
    {
        if (CheckName(f, l))
        {
            Name.first = f;
            Name.last = l;
        }
    }

    public bool CheckName(string f, string l)
    {
        //Check if proper input
        return true;
    }

    public void SetAddress(int sNum, string sName, string c, string s, int zip)
    {
        if(CheckAddress(sNum, sName, c, s, zip))
        {
            Address.streetNumber = sNum;
            Address.streetName = sName;
            Address.city = c;
            Address.state = s;
            Address.zipcode = zip;
        }
    }

    public bool CheckAddress(int sNum, string sName, string c, string s, int zip)
    {
        //Check if proper input
        return true;
    }

    public void SetPhoneNumber()
    {

    }

    public bool CheckPhoneNumber()
    {
        //Check if proper input
        return true;
    }
}
