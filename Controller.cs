﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;

namespace AddressBook
{
    public class Controller
    {
        private List<TextBox> textBoxes;

        public Controller(List<TextBox> tBoxes)
        {
            textBoxes = tBoxes;
            
        }

        public void ClearInput()
        {
            foreach (TextBox box in textBoxes)
            {
                box.Clear();
            }
        }
       
    }
}
