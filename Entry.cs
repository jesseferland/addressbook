﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddressBook
{
    public class Entry
    {    
        public struct Name
        {
            public string first;
            public string last;
        };

       public struct Address
        {
            public string street;
            public string city;
            public string state;
            public string zipCode;
        };

        private string PhoneNumber;
        private Name entryName;
        private Address entryAddress;

        private string entryError;

        public Entry()
        {
            entryError = "None";
        }

       public void SetName(string f, string l)
        {
            if (CheckName(f, l))
            {
                entryName.first = f;
                entryName.last = l;
            }
        }

        public bool CheckName(string f, string l)
        {
            if (OnlyLetters(f) && OnlyLetters(l))
                return true;
            else
                entryError = "Name must contain only letters";
                return false;
        }

        public void SetAddress(string address, string city, string state, string zipString)
        {

            if (CheckAddress(address, city, state, zipString))
            {
                entryAddress.street = address;
                entryAddress.city = city;
                entryAddress.state = state;
                entryAddress.zipCode = zipString;
            }
        }

        public bool CheckAddress(string address, string city, string state, string zip)
        {  
            if (!OnlyLetters(city))
            {
                entryError = "City must only contain letters"; 
                return false;
            }
            if (!OnlyLetters(state))
            {
                entryError = "State/Country must only contain letters";
                return false;
            }

            if (zip.Length == 5)
            {
                foreach (char digit in zip)
                {
                    if (digit < '0' || digit > '9')
                    {
                        entryError = "Zip code must contain 5 digits 0 - 9";
                        return false;
                    }
                }
            }
            else
                entryError = "Zip must contain exactly 5 digits 0 - 9";

            //If all inputs are correct
            return true;
        }

        public void SetPhoneNumber(string pNumString)
        {
            if(CheckPhoneNumber(pNumString))
            {
                PhoneNumber = pNumString;
            }
        }

        public bool CheckPhoneNumber(string pNum)
        {           
            if (pNum.Length != 10)
            {
                entryError = "Phone Numbers must contain exactly 10 digits (Do not include spaces or hyphens)";
                return false;
            }
            
            foreach (char digit in pNum)
            {
                if (digit < '0' || digit > '9') //Character codes 16 - 25
                {
                    entryError = "Phone number must be 10 digits (Do not include a 1 at the beginning)";
                    return false;
                }
            }
            return true;
        }

        public bool OnlyLetters(string toCheck)
        {
            foreach (char c in toCheck)
            {
                if (!Char.IsLetter(c) && !Char.IsWhiteSpace(c)) //Also check for white space in cases of country name, street at the end of address, etc
                    return false;
            }
            return true;
         }

        public string GetErrors()
        {
            return entryError;
        }

        public Entry CreateSearchQuery(string fName, string lName, string address, string city, string state, string zip, string number)
        {
            Entry temp = new Entry();

            temp.entryName.first = fName;
            temp.entryName.last = lName;
            temp.entryAddress.street = address;
            temp.entryAddress.city = city;
            temp.entryAddress.state = state;
            temp.entryAddress.zipCode = zip;
            temp.PhoneNumber = number;

            return temp;
        }

        public bool CompareEntry(Entry toCompare)
        {
            bool match = false;

            if(toCompare.GetName().first != "/" ) 
            {
                if(toCompare.GetName().first == entryName.first)
                    match = true;
                else 
                   return false;
            }

            if (toCompare.GetName().last != "/")
            {
                if (toCompare.GetName().last == entryName.last)
                    match = true;
                else
                    return false;
            }


            if (toCompare.GetPhoneNumber() != "/")
            {
                if (toCompare.GetPhoneNumber() == PhoneNumber)
                    match = true;
                else
                    return false;
            }

            if (toCompare.GetAddress().state != "/")
            {
                if (toCompare.GetAddress().state == entryAddress.state)
                    match = true;
                else
                    return false;
            }

            if (toCompare.GetAddress().city != "/")
            {
                if (toCompare.GetAddress().city == entryAddress.city)
                    match = true;
                else
                    return false;
            }

            if (toCompare.GetAddress().street != "/")
            {
                if (toCompare.GetAddress().street == entryAddress.street)
                    match = true;
                else
                    return false;
            }

            if (toCompare.GetAddress().zipCode != "/")
            {
                if (toCompare.GetAddress().zipCode == entryAddress.zipCode)
                    match = true;
                else
                    return false;
            }

            return match;

        }

#region Getters
        public Name GetName()
        {
            return entryName;
        }

        public Address GetAddress()
        {
            return entryAddress;
        }

        public string GetPhoneNumber()
        {
            return PhoneNumber;
        }
#endregion
    }
}
