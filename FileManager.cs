﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AddressBook
{
    class FileManager
    {
        public FileManager()
        {

        }

        public void Save(List<Entry> book)
        {
            using (XmlWriter writer = XmlWriter.Create("AddressBook.xml"))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Addresses");

                foreach (Entry entry in book)
                {
                    writer.WriteStartElement("Entry");

                    writer.WriteElementString("First", entry.GetName().first);
                    writer.WriteElementString("Last", entry.GetName().last);
                    writer.WriteElementString("Street", entry.GetAddress().street);
                    writer.WriteElementString("City", entry.GetAddress().city);
                    writer.WriteElementString("State", entry.GetAddress().state);
                    writer.WriteElementString("Zip", entry.GetAddress().zipCode.ToString());
                    writer.WriteElementString("PhoneNum", entry.GetPhoneNumber().ToString());

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        public List<Entry> Load()
        {
            List<Entry> book;
            book = new List<Entry>();
             using (XmlReader reader = XmlReader.Create("AddressBook.xml"))
             {
                while (reader.ReadToFollowing("Entry"))
                {
                    Entry temp = new Entry();

                    reader.ReadToDescendant("First");                    
                    string f = reader.ReadElementContentAsString();
                    reader.MoveToAttribute("Last");
                    string l = reader.ReadElementContentAsString();
                    temp.SetName(f, l);

                    reader.MoveToAttribute("Street");
                    string street = reader.ReadElementContentAsString();

                    reader.MoveToAttribute("City");
                    string city = reader.ReadElementContentAsString();
                    reader.MoveToAttribute("State");
                    string state = reader.ReadElementContentAsString();

                    reader.MoveToAttribute("Zip");
                    string zip = reader.ReadElementContentAsString();
                    temp.SetAddress(street, city, state, zip);

                    reader.MoveToAttribute("PhoneNum");
                    string phoneNum = reader.ReadElementContentAsString();
                    temp.SetPhoneNumber(phoneNum);

                    book.Add(temp);                   
                }              
            }

             return book;
        }
    }
   
}
