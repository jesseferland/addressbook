﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddressBook
{
    class View
    {
        private DataGridView bookDisplay;

        public View(DataGridView display)
        {
            bookDisplay = display;
        }

        public void ClearDisplay()
        {
            bookDisplay.Rows.Clear();
        }

        public void UpdateDisplay(Entry toDisplay)
        {
            string[] data = new string[7];
            data[0] = toDisplay.GetName().last;
            data[1] = toDisplay.GetName().first;
            data[2] = toDisplay.GetAddress().street;
            data[3] = toDisplay.GetAddress().city;
            data[4] = toDisplay.GetAddress().state;
            data[5] = toDisplay.GetAddress().zipCode;
            data[6] = toDisplay.GetPhoneNumber();
            bookDisplay.Rows.Add(data);
        }

        public void UpdateDisplay(List<Entry> book)
        {
            ClearDisplay();
            foreach (Entry theEntry in book)
            {
                UpdateDisplay(theEntry);
            }
        }
    }
}
